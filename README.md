# Py Key Sound

This program make your keyboard sounds like a mechanical keyboard <br>
I've tested it only on linux, maybe you need to install some libs <br>
[YouTube Video](https://youtu.be/Jj4nO4qQufE)

## Python

### Windows
[Download 64bit](https://www.python.org/ftp/python/3.9.6/python-3.9.6-amd64.exe) \
[Download 32bit](https://www.python.org/ftp/python/3.9.6/python-3.9.6.exe) \
[Download other versions](https://www.python.org/downloads/windows/)

### MacOS (v10.9 or higher)
[Direktdownload](https://www.python.org/ftp/python/3.11.0/python-3.11.0-macos11.pkg)

### Linux (apt)
```bash
sudo apt install -y python3 python3-pip
```


## Install libs

```bash
pip3 install -r libs
```
<br>
If there are some missing libs, install it with

Linux
```bash
pip3 install LIB
```

Windows
```bash
pip install LIB
```
## Run
Linux
```bash
python3 main.py
```

Windows
```bash
python main.py
```
