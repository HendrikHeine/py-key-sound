from pynput.keyboard import Key, Listener
from _thread import *
import asyncio
import sound as soundClass

sound = soundClass.Sound()
pressedKeys = []

def on_press(key:Key):
    if key in pressedKeys:pass
    else:
        start_new_thread(sound.sound, (key,))
        pressedKeys.append(key)

def on_release(key:Key):
    try:pressedKeys.remove(key)
    except:pass

async def listen():
    with  Listener(on_press=on_press, on_release=on_release) as listener:
        listener.join()

if __name__ == "__main__":
    print("Program running. Have a lot of fun :)")
    asyncio.run(listen())
