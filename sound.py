from pynput.keyboard import Key
import random
import playsound

class Sound:
    def sound(self, key:Key):
        file = None
        try:
            if key == key.backspace:
                file = "back.wav"
            if key == Key.space:
                file = "space2.wav"
            if key == Key.shift_l:
                file = "shift.wav"
            if key == key.alt:
                file = "alt.wav"
            if key == key.alt_gr:
                file = "alt.wav"
            if key == key.enter:
                file = "ent.wav"
            if key == key.ctrl:
                file = "ctrl.wav"
            if key == key.caps_lock:
                file = "caps.wav"
            #if key == key.esc:
            #    file = "Bruh.wav"
        except:pass
        if file == None:
            files = ["key1.wav", "key2.wav", "key3.wav", "key4.wav", "key5.wav", "key6.wav"]
            file = random.choice(files)
        playsound.playsound(f'sounds/Razer Green (Blackwidow Elite) - Akira/{file}')
